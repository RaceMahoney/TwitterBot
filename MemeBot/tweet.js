const Twit = require('twit');
const config = require('./config');
const T = new Twit(config);
const fs = require("fs");


//tweet an image
var tweetMeme = (img, todaySrc, callback) => {
  var b64content = fs.readFileSync(img, {encoding: 'base64'});  //formate the image

  //Let Twitter know you plan to tweet an formmated image
  T.post('media/upload', {media_data: b64content}, uploaded);

  //Upload the image to Twitter
  function uploaded(err, data, response) {
    // This where the tweet is made
    var id = data.media_id_string;
    var tweet = {
      status: 'BOT: Today\'s #MemeOfTheDayis from ' + todaySrc,   //status to go with tweet
      media_ids: [id]
    }
    T.post('statuses/update', tweet, tweeted)     //Post the full tweet
  }

  // Confirm that the tweet went through and was posted
  function tweeted(err, data, response){
    if(err){
      callback("Something went wrong");
    } else {
      callback(undefined,{
        worked: 'It worked!'
      });
    }
  }
}

module.exports = {
  tweetMeme
  };
