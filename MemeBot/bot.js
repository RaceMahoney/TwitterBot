const fs = require('fs');
const reddit = require('./redditExtractor.js');
const twitter = require('./tweet.js');
const rp = require('request-promise');
const cheerio = require('cheerio');
const download = require('image-downloader');


URLS = [
  'https://www.reddit.com/r/MemeEconomy/top/',      //Sunday
  'https://www.reddit.com/r/memes/top/',                //Monday
  'https://www.reddit.com/r/PrequelMemes/top/',         //Tuesday
  'https://www.reddit.com/r/dankmemes/top/',            //Wedensday
  'https://www.reddit.com/r/Memes_Of_The_Dank/top/',          //Thursday
  'https://www.reddit.com/r/wholesomememes/top/',       //Friday
  'https://www.reddit.com/r/PewdiepieSubmissions/top'      //Saturday
];

src = [
  'r/MemeEconomy/',      //Sunday
  'r/memes/',                //Monday
  'r/PrequelMemes/',         //Tuesday
  'r/dankmemes/',            //Wedensday
  'r/Memes_Of_The_Dank/',          //Thursday
  'r/wholesomememes/',       //Friday
  'r/PewdiepieSubmissions/'      //Saturday
];

var date = new Date();    //Date object
var day = date.getDay();  //Get today's day of the week as a value 0-6
var todaySrc = src[day];  //The correct src subreddit for the day
var URL = URLS[day];    //URL for the day
var path = '/home/race/NodePractice/TwitterBot/MemeBot/pics';
console.log('Connecting to website');

//First get the correct link to the first image from today's URL
reddit.getLink(URL, (error, results) => {
  if(error){
    console.log(error);
  } else {
    console.log(results.Link);
    //Add reddit.com to the link to make it valid
    var img_src = reddit.getPic('https://reddit.com' + results.Link, (err, res) => {
      if(err){
        console.log(err);
      } else {
        console.log(res.src);
        //From the reddit link get the image src to be saved locally
        reddit.saveImage(res.src, path, (_err, _res) => {
          if(_err){
            console.log(_err);
          } else {
            console.log('Filed saved to: ' + _res.file);
            //Once the image is saved, find the most recent file created in the directory
            //This must be done since the program will think the file does not
            //exist if it is done to quickly
            reddit.getImage((__err, __res) => {
              if(__err){
                console.log(__err);
              } else {
                console.log(__res.img);
                //With the path to the correct image, tweet it to twitter
                twitter.tweetMeme(__res.img, todaySrc, (T_err, T_res) => {
                  if(T_err){
                    console.log(T_err);
                  } else {
                    console.log(T_res.worked);
                  }
                });
              }
            });
          }
        });
      }
    });
  }
});
