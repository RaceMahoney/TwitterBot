const request = require('request');
const cheerio = require('cheerio');
const download = require('image-downloader');
const fs = require("fs");
const dirContents = require('readdir')
var path = '/home/race/NodePractice/TwitterBot/MemeBot/pics/';

//default header to reddit thinks you are using a browser
var customHeaderRequest = request.defaults({
    headers: {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36'}
});

//Retrive the link from the reddit page
var getLink = (url, callback) => {
  customHeaderRequest.get(url, function(err, resp, body){
    $ = cheerio.load(body);
    links = $('a');
    $(links).each(function(i, link){
      //On each reddit page the 16th 'a' tag contains the needed link
      if(i == 16){
        if($(link).attr('href') == 'https://www.reddit.com/r/gifs/') {
          console.log('Trying again');
          getLink(url, callback);
        } else {
          callback(undefined,{
            Link: ($(link).attr('href'))
          });
        }

      }
    });
  });
}

//Get the pic src from the reddit post
var getPic = (link, callback) => {
  customHeaderRequest.get(link, function(err, resp, body){
    $ = cheerio.load(body);
    images = $('img');

    $(images).each(function(i, img){
      //Make sure that image src obtained is in the correct format
      //If not, run the funtion again
      if(!($(img).attr('src')).includes('https://i.redd.it/')){
        console.log('Trying again');
        getPic(link, callback);
      } else {
        callback(undefined,{
          src: ($(img).attr('src'))
        });
      }
    });
  });
}

//Save the image to the local disk
var saveImage = (img_src, path, callback) => {
    var options = {
      url:  img_src,
      dest: path,
      family: 4
    }

    download.image(options)
      .then(({filename, image}) => {
        callback(undefined, {
          file: (filename),
        });
      })
  }

//Read from the given directory
  var getImage = (callback) => {
    files = dirContents.readSync(path);
    if(typeof files === 'undefined'){
      callback('Could not find dir')
    } else {
      //Iterate through all the files in the directory to find the newest one
      var firstFile = fs.statSync(path+files[0]).ctime
      var index = 0;
      for (var i = 1; i < files.length; i++){
        var currentFile = fs.statSync(path+files[i]).ctime;

        if(firstFile.getMonth() <= currentFile.getMonth()){
          if(firstFile.getDate() < currentFile.getDate()){
            firstFile = currentFile;
            index = i;
          }
        }
      }
      //Appened the full path to the file to be used right away when returend
      callback(undefined,{
        img: path + files[index]
      });
    }
  }

module.exports = {
  getLink,
  getPic,
  saveImage,
  getImage
};
